import mongoose from 'mongoose';

const designSchema = new mongoose.Schema({
    websiteName: {type: String,  required: true},
    icon: {type: String,  required: true},
    footer: {type: String, required: true}
});

designSchema.pre('save', function (next) {
    next();

});


const Design = mongoose.model('Design', designSchema);

export default Design;
