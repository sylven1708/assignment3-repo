import bcrypt from 'bcryptjs';
import User from '../models/user.js';
import Page from "../models/page.js";
import Design from "../models/design.js";

class AuthController {
    constructor() {
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
    }

    rememberUser(req, res, username) {
        req.session.loggedin = true;
        req.session.username = username;
    }

    forgetUser(req, res) {
        req.session.loggedin = false;
        delete req.session.username;
    }

    async getLoginForm(req, res) {
        const allPages = await Page.find({isAdminPage: req.session.loggedin ? true : false}, 'pageName pageCode path');
        const design = await Design.find();

        return res.render('pages/login', {pages: allPages, design: design[0], page: {pageCode: 'login'}});
    }

    async login(req, res) {
        let {username, password} = req.body;
        const user = await User.findOne({username}).exec();
        const design = await Design.find();

        if (user && bcrypt.compareSync(password, user.password)) {
            this.rememberUser(req, res, username);

            return res.redirect('/');
        }

        return res.render('pages/login', {
            errorMessage: "Wrong username or password",
            pages: await Page.find({isAdminPage: req.session.loggedin ? true : false}, 'pageName pageCode path'),
            page: {pageCode: 'login'},
            design: design[0]
        });
    }

    async logout(req, res) {
        this.forgetUser(req, res);

        const isAdmin = req.session.loggedin ? true : false;

        const allPages = await Page.find({isAdminPage: isAdmin}, 'pageName pageCode path');

        const design = await Design.find();

        const page = await Page.findOne({pageCode: 'home'});

        return res.render('pages/page', {pages: allPages, page, isAdmin, design: design[0]});
    }
}

export default AuthController

