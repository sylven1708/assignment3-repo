import Page from "../models/page.js";
import Design from "../models/design.js";
import multer from 'multer';
import path from 'path';

const upload = multer({dest: path.join(path.resolve(), 'src', 'public')})

async function fetchData(req, res, param) {

    const isAdmin = req.session.loggedin ? true : false;

    const allPages = await Page.find({ isAdminPage: isAdmin }, 'pageName pageCode path');

    const design = await Design.find();

    const page = await Page.findOne({ pageCode: param });

    const userPages = await Page.find({ isAdminPage: false }, 'pageName pageCode');

    return {
        isAdmin: isAdmin,
        allPages: allPages,
        userPages: userPages,
        design: design,
        page: page
    };
};

class PageController {

    async getPage(req, res) {

        let pageCode = req.query.pageCode ? req.query.pageCode : 'addpage';

        const { isAdmin, allPages, userPages, design, page } = await fetchData(req, res, pageCode);

        if (isAdmin)
            return res.render(`pages/${page.pageCode}`, {
                pages: allPages,
                userPages: userPages,
                design: design[0],
                page,
                isAdmin
            });

        return res.render('pages/page', { pages: allPages, page, isAdmin, design: design[0] });
    }

    async deletePage(req, res) {
        let pageToDelete = Page.findOne({ pageCode: req.query.pageCode });

        let message = 'Failed to delete';

        if (!pageToDelete.isAdminPage) {
            await Page.deleteOne({ pageCode: req.query.pageCode });
            message = 'Page successfully deleted';
        }

        const { isAdmin, allPages, userPages, design, page } = await fetchData(req, res, 'editpages');

        return res.render(`pages/editpages`, {
            message: message,
            pages: allPages,
            userPages: userPages,
            design: design[0],
            page,
            isAdmin
        });
    }

    async editPage(req, res) {

        const pageToEdit = await Page.findOne({ pageCode: req.query.pageCode });

        const { isAdmin, allPages, userPages, design, page } = await fetchData(req, res, 'editpages');

        if (!isAdmin) {
            return res.render(`pages/page`, {
                pages: allPages,
                userPages: userPages,
                design: design[0],
                isAdmin,
                page
            });
        }

        return res.render(`pages/addpage`, {
            pages: allPages,
            userPages: userPages,
            design: design[0],
            pageToEdit,
            page,
            isAdmin
        });
    }


    async designPage(req, res) {

        await Design.update(req.body);

        const { isAdmin, allPages, userPages, design, page } = await fetchData(req, res, 'editheader');

        return res.render(`pages/editheader`, {
            pages: allPages,
            userPages: userPages,
            design: design[0],
            page,
            isAdmin
        });
    }

    async postImage(req, res) {

    }

    async postPage(req, res) {
        const { isAdmin, allPages, userPages, design, page } = await fetchData(req, res, 'addpage');

        if (!isAdmin) {
            return res.render(`pages/page`, {
                pages: allPages,
                userPages: userPages,
                design: design[0],
                isAdmin,
                page
            });
        }

        var result = '';

        // if the returned value is -1, the add function is to be performed
        if (req.rawHeaders[31].indexOf("edit") == -1) {

            await Page.create(req.body);
            result = 'You have successfully created a new page!';
        } else {
            //get which page is to be updated from the url by regex
            await Page.updateOne({ pageCode: req.rawHeaders["31"].match("\=(.*)")[1] }, req.body, { upsert: true });
            result = 'You have successfully updated the page!';
        }

        return res.render('pages/addpage', {
            message: result,
            pages: allPages,
            page: page,
            design: design[0],
            isAdmin
        });
    }
}

export default PageController
export {upload}