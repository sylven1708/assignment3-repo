import express from 'express';
import PageController from "../controllers/page.js";
import {upload} from "../controllers/page.js";

const router = express.Router();
const pageController = new PageController();

router
    .get('/', pageController.getPage)
    .get('/delete', pageController.deletePage)
    .get('/edit', pageController.editPage)
    .post('/edit', pageController.editPage)
    .post('/page', upload.single('image'), pageController.postPage)
    .post('/design', pageController.designPage);

export default router;