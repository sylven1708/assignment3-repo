--Welcome--
The Project is a basic web shop which allows you to purchase different items and add product to cart.
It has functionality to generate receipt once the order is processed. 
The purchase records,authentication details, user details etc are all stored inn mongo DB.

Steps to run project:

Setup MongoDB for project:
1. Install Node.js (This is required to run the project as whole)
2. Install MongoDB Node.js driver
		-- npm install mongodb
3. Create Mongo Atlas Cluster
4. Create MongoDB functions(predefined in handbook) to use in the  project
5. Refer below mentioned link for basic setup
		-- https://www.mongodb.com/blog/post/quick-start-nodejs-mongodb--how-to-get-connected-to-your-database
		
Setup for website: 
1. Install Express Generator(This should help in running the ejs files in project )
		-- npm -g express-generator
2. Install every dependency
		-- npm install
3. Open app.js file in ~/Project/src/app.js and start the project with
		-- npm start
4. Basic startup steps are mentioned in link below
		-- https://closebrace.com/tutorials/2017-03-02/the-dead-simple-step-by-step-guide-for-front-end-developers-to-getting-up-and-running-with-nodejs-express-and-mongodb